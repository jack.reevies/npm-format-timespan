## Format Timespan

Format a plain millisecond timespan into something more readable

### Example

49113031000 = 1 year, 6 months, 20 days, 13 hours, 30 minutes, 31 seconds

Accuracy | formatMsSpanWords | formatMsSpanMillitary
-|-|-
Year | 2 years | 02:00:00:00:00:00
Month | 1 year, 7 months | 01:07:00:00:00:00
Day | 1 year, 6 months, 21 days | 01:06:21:00:00:00
Hour | 1 year, 6 months, 20 days, 14 hours | 01:06:20:14:00:00
Minute | 1 year, 6 months, 20 days, 13 hours, 31 minutes | 01:06:20:13:31:00
Second | 1 year, 6 months, 20 days, 13 hours, 30 minutes, 31 seconds | 01:06:20:13:30:31

### formatMsSpanWords(ms: number, accuracy: TimeUnit = TimeUnit.Millis)

Generates output using words such as `5 days, 2 hours, 3 seconds`

Supports rounding by setting accuracy as the smallest time unit we're interested in.

Automatically removes units with a value of 0 (ie, `0 minutes` was omitted from the above example)

### formatMsSpanMillitary(ms: number, accuracy: TimeUnit = TimeUnit.Millis, smallestUnit: TimeUnit.Seconds)

Generates output in a millitary like style `05:02:00:03`

Rounding is supported in the same way as `formatMsSpanWords`

Supports `smallestUnit` to drop off any units smaller than it (without rounding). For example `05:02:00:03` becomes `05:02:00` if `smallestUnit` is set as `TimeUnit.Minute`

Note: `smallestUnit` and `accuracy` have no baring on eachother. Setting `accuracy` to `TimeUnit.Day` and `smallestUnit` to `TimeUnit.Second` produces `05:00:00:00`. This can be useful in cases where consistency is required