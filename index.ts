const MS: { [spanName: string]: number } = {
  YEAR: 0,
  MONTH: 0,
  DAY: 0,
  HOUR: 0,
  MINUTE: 0,
  SECOND: 1000,
  MILLISECOND: 1
}

MS.MINUTE = 60 * MS.SECOND
MS.HOUR = 60 * MS.MINUTE
MS.DAY = 24 * MS.HOUR
MS.YEAR = 365.25 * MS.DAY
MS.MONTH = MS.YEAR / 12

export enum TimeUnit {
  Milli = 6, Second = 5, Minute = 4, Hour = 3, Day = 2, Month = 1, Year = 0
}

/**
 * Format a timespan to a wordy display like (2 days, 5 hours, 1 second)
 * @param ms a timespan represented in milliseconds
 * @param accuracy The lowest time unit we care for (ie, if minutes, 30 seconds will be counted as 1 minute)
 */
export function formatMsSpanWords(ms: number, accuracy: TimeUnit = 6) {
  let tmp = ms
  const counts: number[] = []

  const spanKeys = Object.keys(MS)
  spanKeys.forEach(spanKey => {
    const span = MS[spanKey]
    const count = Math.floor(tmp / span)
    counts.push(count)
    tmp -= count * span
  })

  if (accuracy < 6) {
    let msBelow = 0
    for (let i = 6; i > accuracy; i--) {
      const count = counts[i] * MS[spanKeys[i]]
      msBelow += count
      counts[i] = 0
    }
    if (msBelow >= MS[spanKeys[accuracy]] / 2) {
      counts[accuracy]++
    }
  }

  const builder: string[] = []

  spanKeys.forEach((spanKey, i) => {
    const count = counts[i]
    if (!count && !builder.length) return
    builder.push(`${count} ${spanKey.toLowerCase()}${count === 1 ? '' : 's'}`)
  })

  for (let i = builder.length - 1; i >= 0; i--) {
    const part = builder[i];
    if (part.startsWith('0')) {
      builder.splice(i, 1)
    }
  }

  if (!builder.length) {
    return `0 ${spanKeys[accuracy].toLowerCase()}s`
  }

  return builder.join(', ')
}

/**
 * Format a timespan to a millitary like display (23:00:01)
 * @param ms a timespan represented in milliseconds
 * @param accuracy The lowest time unit we care for (ie, if minutes, 30 seconds will be counted as 1 minute)
 * @param smallestUnit The lowest unit of time to display (all inferior units are dropped - not rounded)
 */
export function formatMsSpanMillitary(ms: number, accuracy: TimeUnit = 6, smallestUnit: TimeUnit = 5) {
  let tmp = ms
  const counts: number[] = []

  const spanKeys = Object.keys(MS)
  spanKeys.forEach(spanKey => {
    const span = MS[spanKey]
    const count = Math.floor(tmp / span)
    counts.push(count)
    tmp -= count * span
  })

  if (accuracy < 6) {
    let msBelow = 0
    for (let i = 6; i > accuracy; i--) {
      const count = counts[i] * MS[spanKeys[i]]
      msBelow += count
      counts[i] = 0
    }
    if (msBelow >= MS[spanKeys[accuracy]] / 2) {
      counts[accuracy]++
    }
  }

  const builder: string[] = []

  let spliceFrom = smallestUnit + 1
  spanKeys.forEach((spanKey, i) => {
    const count = counts[i]
    if (!count && !builder.length) {
      spliceFrom--
      return
    }
    builder.push(count.toString().padStart(2, '0'))
  })

  if (tmp > 0) {
    builder.push(tmp.toString().padStart(2, '0'))
  }

  if (!builder.length) {
    return '00'
  }

  builder.splice(Math.max(spliceFrom, 0), builder.length)
  return builder.join(':') || '00'
}